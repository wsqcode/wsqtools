package com.wsq.utils.qrcode;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author wsq
 * @Package com.wsq.utils.qrcode
 * @Description: 二维码生成器工具类
 * @Date Created by wsq on 2017/12/28上午1:35.
 * @Modified By:
 */
@Controller
public class QRCodeController {
    /**
     * 获得二维码
     * @param request
     * @param response
     */
    @RequestMapping(value = "phoneversion/getTwoDimension",method={RequestMethod.POST,RequestMethod.GET})
    public void getTwoDimensionForIOSs(HttpServletRequest request, HttpServletResponse response){
        try {
            QRCodeUtil.getTwoDimension("https://bbs.hupu.com/bxj", response, 300, 300);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @RequestMapping("/downloadIOSAPPQRCode")
    public ResponseEntity<byte[]> downloadIOSAPPController(/*@RequestParam(required = true)String type*/)
            throws Exception{
//        InputStream is = this.getClass().getClassLoader().getResourceAsStream("app.properties");
//        Properties props = new Properties();
//        props.load(is);
//        String appId = (String)props.get(type);
//        String url = "" + appId;
        return QRCodeUtil.getResponseEntity("https://www.zhibo8.cc", "F:\\logo.png",true,"png", 300, 300);
    }


    @RequestMapping("/showQrcode")
    public void showQrcode(HttpServletRequest request, HttpServletResponse response)
            throws Exception{
        QRCodeUtil.showQrcode("https://www.zhibo8.cc", "",response,false,"png", 300, 300);
    }


    @RequestMapping("/SaveQrCode")
    public void SaveQrCode(HttpServletRequest request, HttpServletResponse response)
            throws Exception{
        boolean flag=QRCodeUtil.SaveQrCode("https://www.zhibo8.cc", "",false,"png", 300, 300,"D:\\","qrcode");
        System.err.println("flag==" + flag);

    }

//    前端显示:
//
//    <!DOCTYPE html>
//    <html lang="en">
//    <head>
//        <meta charset="UTF-8" />
//        <title>首页</title>
//    </head>
//    <body>
//    <p>二维码图片1:</p>
//    <div><img src="/phoneversion/getTwoDimension" alt="" /></div>
//
//    <p>二维码图片2:</p>
//    <img src="/downloadIOSAPPQRCode"/><a href="/downloadIOSAPPQRCode">下载</a>
//
//    <p>二维码图片3:</p>
//    <img src="/showQrcode"/><a href="/showQrcode">下载</a>
//    </body>
//    </html>

}
