package com.wsq.utils.http;

import org.apache.commons.codec.binary.Base64;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * @Author wsq
 * @Package com.wsq.utils.http
 * @Description: HTTP请求工具类
 * @Date Created by wsq on 2018/1/2上午11:20.
 * @Modified By:
 */
public class HttpUtil {

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    /**
     * @param url 接口url
     * @return
     */
    public static String get(String url) {
        return HttpUtil.get(url, false);
    }

    /**
     * @param url     接口url
     * @param charset 编码格式
     * @return
     */
    public static String get(String url, String charset) {
        return HttpUtil.get(url, false, charset);
    }

    /**
     * @param url   接口url
     * @param isSSL 是否为https请求
     * @return
     */
    public static String get(String url, boolean isSSL) {
        return HttpUtil.get(url, isSSL, "utf-8");
    }

    /**
     * 功能：以http Base认证的方式调用
     *
     * @param url      接口url
     * @param userName 认证用户名
     * @param passWord 认证密码
     * @return
     */
    public static String get(String url, String userName, String passWord) {
        return HttpUtil.get(url, false, "utf-8", userName, passWord);
    }

    /**
     * 发送post请求
     *
     * @param url    接口url
     * @param params 参数
     * @param isSSL  是否为https请求
     * @return
     */
    public static String post(String url, String params, Boolean isSSL) {
        return HttpUtil.post(url, params, isSSL);
    }

    /**
     * 发送get请求
     *
     * @param url      请求的url
     * @param isSSL    是否为https请求
     * @param charset  编码
     * @param userName 认证用户名
     * @param passWord 认证密码
     * @return
     */
    private static String get(String url, boolean isSSL, String charset, String userName, String passWord) {

        String result = "";
        BufferedReader in = null;
        String userPas = userName + ':' + passWord;

        try {
            /*
            Properties prop = System.getProperties();
			prop.put("proxySet","true"); 
			prop.put("proxyHost","10.5.3.9"); 
			prop.put("proxyPort","80");
			 */

            // 跳过https证书验证
            trustAllHosts();

            String urlNameString = url;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            if (isSSL) {
                connection = (HttpsURLConnection) connection;
                ((HttpsURLConnection) connection).setHostnameVerifier(DO_NOT_VERIFY);
            }
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "text/html; charset=" + charset);
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            connection.setRequestProperty("Authorization", "Basic " + Base64.encodeBase64String(userPas.getBytes("utf-8")));
            // 建立实际的连接
            connection.setConnectTimeout(1000);// 设置连接超时时间，单位毫秒
            connection.setReadTimeout(1000);// 设置读取数据超时时间，单位毫秒
            connection.connect();// 打开连接端口

            // 定义 BufferedReader输入流来读取URL的响应数据
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
        } finally {
            // 使用finally块来关闭输入流
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }


    /**
     * 发送get请求
     *
     * @param url   链接地址
     * @param isSSL 是否是https协议
     * @return 返回响应字符串
     */
    private static String get(String url, boolean isSSL, String charset) {

        String result = "";
        BufferedReader in = null;
        try {
            /*
            Properties prop = System.getProperties();
			prop.put("proxySet","true"); 
			prop.put("proxyHost","10.5.3.9"); 
			prop.put("proxyPort","80");
			*/
            trustAllHosts();

            String urlNameString = url;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            if (isSSL) {
                connection = (HttpsURLConnection) connection;
                ((HttpsURLConnection) connection).setHostnameVerifier(DO_NOT_VERIFY);
            }
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.setConnectTimeout(1000);
            connection.connect();
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
        } finally {
            // 使用finally块来关闭输入流
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 发送post请求
     *
     * @param reqUrl 请求url
     * @param params 请求参数
     * @param isSSL  是否为https
     * @return
     */
    private static String post(String reqUrl, String params, boolean isSSL) {
        HttpURLConnection connection = null;
        String responseContent = null;
        try {
            trustAllHosts();
            URL url = new URL(reqUrl);
            connection = (HttpURLConnection) url.openConnection();
            if (isSSL) {
                connection = (HttpsURLConnection) connection;
                ((HttpsURLConnection) connection).setHostnameVerifier(DO_NOT_VERIFY);
            }
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(1000);
            connection.setDoOutput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.connect();
            OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream(), "utf-8");
            osw.write(params);
            osw.flush();
            osw.close();

            InputStream in = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(in));
            String tempLine = rd.readLine();
            StringBuffer tempStr = new StringBuffer();
            String crlf = System.getProperty("line.separator");
            while (tempLine != null) {
                tempStr.append(tempLine);
                tempStr.append(crlf);
                tempLine = rd.readLine();
            }
            responseContent = tempStr.toString();
            rd.close();
            in.close();
        } catch (IOException e) {
            System.out.println("发送POST请求出现异常！" + e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return responseContent;
    }



    private static void trustAllHosts() {

        // Create a trust manager that does not validate certificate chains，创建一个不验证证书链的信任管理器
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {

            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {

            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
