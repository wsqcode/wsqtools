package com.wsq.utils;

import org.apache.commons.codec.binary.Base64;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @Author wsq
 * @Package com.wsq.utils.util
 * @Description: Base64实现加密、解密
 * @Date Created by wsq on 2018/1/2下午1:28.
 * @Modified By:
 */
public class Base64Utils {

    /**
     * Base64加密
     * @param str 传入需要加密字符串
     * @return
     */
    public static String encode(String str) {
        Base64 base64 = new Base64();
        byte[] b = null;
        String s = null;
        try {
            b = str.getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (b != null) {
            s = base64.encodeToString(b);
        }
        return s;
    }

    /**
     * Base64解密
     * @param str 传入需要解密字符串
     * @return
     */
    public static String decode(String str) {
        Base64 base64 = new Base64();
        String result = null;
        if (str != null) {
            try {
                result = new String(base64.decode(str), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 测试main方法
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String s = "password";

        String base64 = encode(s);
        System.out.println("password加密后：" + base64);

        String fromBase64 = decode(base64);
        System.out.println("password解密后：" + fromBase64);

        System.out.println("password解密后：" + decode("cGFzc3dvcmQ"));

    }
}
